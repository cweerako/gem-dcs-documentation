# How to React to an Alarm

####In case you receive a notification of an alarm, you have to:



* Connect to the DCS and open the alarm screen
* Identify the variable involved 
* Understand if the variable is in warning state or in error state 
* Act in a proper way --> in the DCS Twiki

you can find a list of common errors and a short description of the actions
to be taken on this page [“this page”](/shifterguide/common-errors-and-actions.md)

!!! info "If you don’t understand the alarm / the reason call an [“expert”](../contact.md)"	
	
???+ Example "Example 1 :- Standard HV channel in error"
	**You received this message:**
	```
		Channel Error cms_gem_dcs_1:CAEN/GEM_CAEN_HV/board02/channel001.actual.OvC Channel Error (bool alert activated);
		Message sent from system cms_gem_dcs_1: @ DCS-S2F16-07-16
	```
	**What does it mean?**
	
	* cms_gem_dcs_1:CAEN/GEM_CAEN_HV/board02/channel001 -> Name of the channel in error -> From the twiki: HV channel of GEMINI27-Layer2
	* .actual.OvC -> Type of error -> OvC = Overcurrent
	
	**What to do?**
	
	- Open the DCS and search for the right chamber
	- Which is the status of the HV channel?
	
		1. The OvC can be transient -> the channel returns to the normal condition
		2. If the OvC is not transient -> the channel trips
	
	**In case 1):**
	
	* Check the alarm screen -> the OvC error should have direction WENT
	* Acknowledge it
	
	**In case 2):**
	
	* Check the alarm screen  you should find both the OvC error and the Trip error, with direction CAME
	* Check if the gas is correctly flowing into the chamber -> GAS tab, go with the mouse over the correct flowcell rectangle
	
		* If the gas is NOT-OK, see next slides (you should have received a notification too)
		* If the gas is OK, try to restart the HV on the chamber -> from the CAEN tab, search for GEMINI27-Layer2, press ON HV and wait until the ramping up is complete
			* If the chamber remains ON, acknowledge all the alarms
			* If the chamber trips again, call the expert
	
	!!! info "In any case, at the end of you intervention, write a detailed ELOG, with a description of the problem and the actions you have taken!"

???+ Example "Example 2 :- LV channel in error"
	**You received this message:**
	```
		Channel Error
		cms_gem_dcs_1:CAEN/GEM_CAEN_LV/branchController00/easyCrate0/easyBoard17/channel003.actual.unplugged Channel Error (bool alert activated);
		Channel Error
		Message sent from system cms_gem_dcs_1: @ DCS-S2F16-07-16
	```
	**What does it mean?**
	
	* cms_gem_dcs_1:CAEN/GEM_CAEN_LV/branchController00/easyCrate0/easyBoard17/channel003 -> Name of the channel in error -> From the Twiki: LV channel GEB/VFAT of GEMINI01 – Layer2
	* .actual.unplugged -> Type of error -> OvC = Unplugged
	
	**What to do?**
	
	- Open the DCS and search for the right chamber
	- Which is the status of the LV channel? -> The channel should be in error (in red), probably also the other two channels of the same chamber are in error
	- Check the alarm screen -> you should find the error with direction CAME
	- Clear the LV alarms with the button on the DCS main panel
	- Try to restart the LV -> from the CAEN tab, search for GEMINI01-Layer2, press ON LV and wait until the LV is ON
		- If it stays ON, acknowledge the alarms
		- If it goes in error again, call the expert
	
	!!! info "In any case, at the end of you intervention, write a detailed ELOG, with a description of the problem and the actions you have taken!"

!!! info "N.B. The unplugged error on the LV can usually happen in two situations:"
	1. When the system is completely restarted with the LV ON button on the main panel -> in this case, following the procedure above is enough to solve the problem
	2. When the MAO goes off -> in this case, all the LV channels of all the chambers go in unplugged error all together -> in this case:
		- Clear the LV alarms with the button on the main panel
		- Try to restart the MAO (see slide 20-22)
			- If the MAO stays ON, follow the procedure above for all the chambers
			- If the MAO goes OFF again, call the expert
			
???+ Example "Example 3 :- Gas Flowcell in error"
	**You received this message:**
	```
		Alert: check the input flow cms_gem_dcs_1:CMSGEM_Di_FE6102Ch4.Value= 0.199999999999 (multi range alert triggered);
		Message sent from system cms_gem_dcs_1: @ DCS-S2F16-07-16
	```
	**What does it mean?**
	
	* cms_gem_dcs_1:CMSGEM_Di_FE6102Ch4.Value= 0.199999999999 -> Name of the channel in error -> From the Twiki: Flowcell Ch4, GEMINI 29-30
	* check the input flow + CMSGEM_Di_FE6102Ch4 -> Type of error -> Wrong input flow on that channel
	
	**What to do?**
	
	- Open the DCS and search for the right flowcell
	- Go with the mouse over it (or open the Gas Settings table) and understand which is the current input gas flow value
		- If the gas flow is back to the normal value, it was only a glitch in the DIP readings -> acknowledge the alarms
		- If it is not back to the normail value, you potentially have a gas problem
	
	**In Case 2):**
		- Understand if it is a warning (yellow) or an alert (red)
			- If it is a warning, you still have gas flow in the chambers, you can wait a bit to see if the normal flow is restored -> if it is not, call the GEM DCS expert or the GEM GAS expert to understand if this gas flow can be accepted for a while.
			- If it is an error and it is not a fluctuation (i.e. in 2-3 min is not returned to a safe range), immediately the GEM DCS expert or the GEM GAS expert! Arrange with them the next step, which can be calling the CMS GAS expert (or the Gas Piquet)***
		
		** N.B. If your flow (or pressure) is really going to zero and you don’t manage to contact the GEM expert, call directly the CMS GAS expert (or Gas Piquet)**
	
	If the problem is solved, just acknowledge the alarm. 
	
	In case the problem is not solved the chambers should be switched OFF.
	
	!!! info "In any case, at the end of you intervention, write a detailed ELOG, with a description of the problem and the actions you have taken!"
